<nav class="main-footer-menu">
	<h3 class="heading-style-3 footer-subtitle"><?php _e('Na skróty', 'Ecoshine'); ?></h3>
	<?php wp_nav_menu(array("theme_location" => 'carcare_footer', 'container' => false)); ?>
</nav>