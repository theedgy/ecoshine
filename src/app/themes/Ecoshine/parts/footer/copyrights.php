
<div class="footer-copyrights">
	<div class="footer-copyrights-wrapper">
		<?php echo get_field('footer_copyrights', 'options') .' &#169; '. date("Y"); ?>
		<a href="<?php echo  get_bloginfo('url') ?>">ecoshine.pl</a>
	</div>
</div>