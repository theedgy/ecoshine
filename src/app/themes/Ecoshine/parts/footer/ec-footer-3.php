
<nav class="main-footer-menu">
	<h3 class="heading-style-3 footer-subtitle"><?php _e('Na skróty', 'Ecoshine'); ?></h3>
	<?php wp_nav_menu(array("theme_location" => 'footer', 'container' => false)); ?>

	<h3 class="heading-style-3 footer-subtitle"><?php echo wp_get_nav_menu_object(8)->name ?></h3>

	<?php wp_nav_menu(array("menu" => 8)); ?>

</nav>