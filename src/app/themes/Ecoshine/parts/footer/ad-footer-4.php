<div class="footer-fast-contact">
	<h3 class="heading-style-3 footer-subtitle"><?php _e('Szybki kontakt', 'Ecoshine'); ?></h3>
	<?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]') ?>
</div>

<div class="footer-socials">
	<h3 class="heading-style-3 footer-subtitle"><?php _e('Bądź na bieżąco', 'Ecoshine'); ?></h3>
	<ul class="main-menu__socials">
		<?php
		$socials = get_field('socialki_autodetailing', 'options');
		foreach ($socials as $social) { ?>

			<li>
				<a href="<?php echo $social['link'] ?>" class="main-menu__socials-single" target="_blank"
				   title="<?php echo $social['nazwa'] ?>">
					<?php echo wp_get_attachment_image($social['ikona']['id'], false, false, array('class' => 'svg')); ?>
				</a>
			</li>

		<?php } ?>

	</ul>
</div>
