
<div class="footer-address">
	<h3 class="heading-style-3 footer-subtitle"><?php the_field('adres_tytul', 'options'); ?></h3>
	<?php the_field('adres_tekst', 'options'); ?>
</div>

<nav class="main-cennik-menu">
	<h3 class="heading-style-3 footer-subtitle"><?php _e('Cennik', 'Ecoshine'); ?></h3>
	<?php wp_nav_menu(array("menu" => 'menu-cennik-footer', 'container' => false), 'container_class'); ?>
</nav>
