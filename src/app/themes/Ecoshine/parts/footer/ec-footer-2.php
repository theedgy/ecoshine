
<div class="footer-marketing">
	<h3 class="heading-style-3 footer-subtitle"><?php the_field('text_tytul', 'options'); ?></h3>
	<?php the_field('tekst_zawartosc', 'options'); ?>
</div>
<div class="footer-latest mobile-hidden">
	<h3 class="heading-style-3 footer-subtitle"><?php _e('Co u nas słychać', 'Ecoshine'); ?></h3>
	<?php
	$the_query = new WP_Query(
		array(
			'post_type' => 'post',
			'posts_per_page' => '3'
		)
	);

	if ($the_query->have_posts()) : ?>

		<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

			<a href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>

		<?php endwhile;
		wp_reset_postdata();

	endif; ?>

</div>