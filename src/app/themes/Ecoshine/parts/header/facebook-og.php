<!-- Facebook SDK -->
<meta property="og:title" content="<?php echo is_single() ? the_title() : get_bloginfo('title'); ?>"/>
<meta property="og:url" content="<?php echo esc_url(get_the_permalink()); ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:description" content="<?php echo get_the_content() ? get_the_content() : 'Pranie i czyszczenie tapicerki meblowej - profesjonalne wykonanie usługi w atrakcyjnej cenie tylko w Ecoshine Wrocław. Zapraszamy do współpracy!'; ?>"/>
<meta property="og:site_name" content="Ecoshine">
<meta property="og:locale" content="pl_PL"/>
<?php $image = get_post_thumbnail_id() ? get_post_thumbnail_id() : get_field('logo_dla_social_media', 'options')['id'];
$size = get_post_thumbnail_id() ? 'background' : 'sm-width';
if ($image) :?>
	<meta property="fb:app_id" content="1708737009182799"/>
	<meta property="og:image"
	      content="<?php echo wp_get_attachment_image_src($image, $size)[0] ?>"/>
	<meta property="og:image:width"
	      content="<?php echo wp_get_attachment_image_src($image, $size)[1] ?>"/>
	<meta property="og:image:height"
	      content="<?php echo wp_get_attachment_image_src($image, $size)[2] ?>"/>
	<meta property="og:image:alt"
	      content="<?php echo !empty(get_post_meta($image, '_wp_attachment_image_alt', true)) ? get_post_meta($image, '_wp_attachment_image_alt', true) : get_bloginfo('title'); ?>"/>
<?php endif; ?>