<?php
$display = !empty($display) ? $display : get_field('page_additional_content_display');
$content = !empty($content) ? $content : get_field('page_additional_content');

if ($display) : ?>
	<div class="custom-grid additional-content">
		<?php echo $content ?>
	</div>

<?php endif ?>
