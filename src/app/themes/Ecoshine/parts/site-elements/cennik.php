<?php $cennik = get_field('menu_cennik', 'options');

if (is_front_page() || is_page(6)): ?>

	<div class="menu-cennik">

		<?php foreach ($cennik as $value) : ?>

			<a href="<?php echo $value['link']['url'] ?>" class="menu-cennik__single animated" <?php echo !empty($value['link']['target']) ? 'target="'.$value['link']['target'].'"' : ''; ?>>

				<span class="menu-cennik__single-title"><?php echo $value['link']['title'] ?></span>

				<?php echo wp_get_attachment_image($value['obrazek']['ID'], 'cennik-tiles', false, array('class' => 'menu-cennik__single-image svg')) ?>

			</a>

		<?php endforeach; ?>

	</div>

<?php endif ?>