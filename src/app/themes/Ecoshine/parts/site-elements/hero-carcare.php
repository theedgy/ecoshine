<?php
$bg = get_the_post_thumbnail_url(get_the_ID(), 'hero') ? get_the_post_thumbnail_url(get_the_ID(), 'hero') : get_field('ad_obrazek_hero', 'options')['sizes']['hero'];
?>

<div class="hero">
    <div class="hero-image"
         style="background-image: url(<?php echo $bg ?>);
                 background-position: center <?php echo get_field('ad_przesuniecie_pionowe') ? get_field('ad_przesuniecie_pionowe') : '50'; echo '%' ?>;">
    </div>

    <div class="video-header">

        <div class="video-header_wrapper">

            <a target="_blank" href="<?php the_field('ad_link_yt', 'options'); ?>"
               class="video-header_youtube">
                <?php the_field('ad_odnosnik_yt', 'options'); ?>
            </a>

            <video autoplay muted loop
                   poster="<?php echo get_field('ad_obrazek_hero', 'options')['url']; ?>"
                   class="video-header-video">
                <source src="<?php echo get_field('ad_plik_mp4', 'options')['url']; ?>" type="video/webm">
                <source src="<?php echo get_field('ad_plik_webm', 'options')['url']; ?>" type="video/mp4">
            </video>

        </div>

    </div>

</div>
