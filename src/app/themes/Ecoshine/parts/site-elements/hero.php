<?php
$bg = get_the_post_thumbnail_url(get_the_ID(), 'hero') ? get_the_post_thumbnail_url(get_the_ID(), 'hero') : get_field('obrazek_hero', 'options')['sizes']['hero'];
$front = is_front_page() ? true : false;
?>

<div class="hero">
	<div class="hero-image <?php echo $front ? '' : 'not-front' ?>"
	     style="background-image: url(<?php echo $bg ?>); background-position: center <?php echo get_field('przesuniecie_pionowe') ? get_field('przesuniecie_pionowe') : '50'; echo '%' ?>;"></div>

	<?php if ($front) : ?>

		<div class="video-header">

			<div class="video-header_wrapper">

				<a target="_blank" href="<?php the_field('link_yt', 'options'); ?>"
				   class="video-header_youtube">
					<?php the_field('odnosnik_yt', 'options'); ?>
				</a>

				<video autoplay muted loop
				       poster="<?php echo get_field('obrazek_hero', 'options')['url']; ?>"
				       class="video-header-video">
					<source src="<?php echo get_field('plik_mp4', 'options')['url']; ?>" type="video/webm">
					<source src="<?php echo get_field('plik_webm', 'options')['url']; ?>" type="video/mp4">
				</video>

			</div>

		</div>

	<?php endif; ?>

</div>
