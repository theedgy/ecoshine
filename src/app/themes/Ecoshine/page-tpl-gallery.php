<?php
/**
 * Template Name: Szablon Galerii
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */

get_header(); the_post(); ?>

    <main class="content full-width">
        <div class="container">
            <div class="gallery-sliders">

                <?php $slidery = get_field('slidery'); $counter = 0;?>

                <?php foreach ($slidery as $slider) { $counter++?>

                <div class="sliders-single">

                    <h2 class="slider-title">
                        <?php echo $slider['nazwa_slidera']; ?>
                    </h2>

                    <div class="slider-single slider-single-<?php echo $counter; ?>">

                        <?php
                        $slider_rel = uniqid();
                             foreach ($slider['slider'] as $sslider) { ?>
                        <a href="<?php echo $sslider['obrazek']['sizes']['swipebox-image']; ?>"
                           class="slider-image"
                           rel="<?php echo $slider_rel ?>"
                           data-lightbox="l-gallery"
                           style="background-image: url(<?php echo wp_get_attachment_image_src($sslider['obrazek']['id'], 'slider-duza')[0]; ?>);">

                        </a>
                        <?php } ?>

                    </div>

                    <div class="slider-nav slider-nav-<?php echo $counter; ?>">

                        <?php 
                             foreach ($slider['slider'] as $sslider) { ?>
                        <?php echo wp_get_attachment_image($sslider['obrazek']['id'], 'slider-mala'); ?>
                        <?php } ?>

                    </div>
                </div>

                <?php } ?>

            </div>
        </div>

    </main>

    <?php get_footer(); ?>
