<?php
/**
 * Template Name: Szablon Car Care
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */

get_header('carcare');
the_post();

get_theme_part('site-elements/hero-carcare');
?>

<main class="content full-width content--carcare">

    <div class="content-container animated">
        <?php the_content(); ?>
    </div>

    <div class="content-autodetailing animated">

        <?php $boxes = get_field('pojedynczy_wiersz'); ?>

        <?php if (isset($boxes)) : $linked = false; ?>

            <div class="carcare-boxes">

                <?php foreach ($boxes as $box) : ?>


                    <article
                            class="carcare-single animated <?php echo $box['czy_pelna_szerokosc'] === 'tak' ? 'fullwidth' : ''; ?>">

                        <?php if ($box['czy_link'] === 'tak') :
                        $linked = true; ?>

                        <a href="<?php echo $box['link']; ?>">

                            <?php endif; ?>

                            <div class="carcare-image">

                                <?php if ($box['czy_obrazek'] == 'obrazek') : ?>
                                    <div class="carcare-image__wrapper"
                                         style="background-image: url(<?php echo wp_get_attachment_image_src($box['image']['id'], 'carcare')[0] ?>);">

                                    </div>

                                <?php elseif ($box['czy_obrazek'] == 'video') : ?>

                                    <video autoplay loop muted width="<?php echo $box['plik_video']['width'] ?>" height="<?php echo $box['plik_video']['height'] ?>">
                                        <source src="<?php echo $box['plik_video']['url'] ?>" type="video/mp4">
                                        <?php echo __('Twoja przeglądarka nie wspiera tagu video', 'ecoshine') ?>
                                    </video>

                                <?php elseif ($box['czy_obrazek'] == 'slider') : ?>

                                    <?php $slides = $box['slider_obrazkow']; ?>

                                    <div class="carcare-image--slider">

                                        <?php
                                        $slider_rel = uniqid();

                                        foreach ($slides as $slide): ?>

                                            <a
                                                    href="<?php echo $slide['sizes']['swipebox-image']; ?>"
                                                    rel="<?php echo $slider_rel ?>"
                                                    data-lightbox="l-gallery"
                                                    class="carcare-image--slider-single"
                                                    style="background-image: url(<?php echo $slide['sizes']['carcare']; ?>);">

                                                <!--												<img src="--><?php //echo $slide['sizes']['carcare']; ?><!--" alt="Slider image"-->
                                                <!--												     class="carcare-image--slider-image">-->

                                            </a>

                                        <?php endforeach; ?>

                                    </div>

                                <?php endif; ?>

                            </div>
                            <div class="carcare-content-wrapper">
                                <?php if ($box['tytul_boxa']) : ?>
                                    <p class="heading-style-2 h2auto">
                                        <?php echo $box['tytul_boxa']; ?>
                                    </p>
                                <?php endif; ?>
                                <div class="carcare-content-wrapper-text">
                                    <?php echo $box['tresc_boxa']; ?>
                                </div>
                                <?php if ($box['czy_link'] === 'tak') : ?>
                                    <span href="#" class="btn btn-rounded"><?php _e('Szczegóły', 'Ecoshine') ?></span>
                                <?php endif; ?>
                            </div>


                            <?php if ($box['czy_link'] === 'tak') :
                            $linked = true; ?>
                        </a>


                    <?php endif; ?>
                    </article>
                <?php endforeach; ?>
            </div>

        <?php endif ?>
    </div>

    <?php if (is_page(471)) { ?>
        <div class="contact-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2506.5825405226574!2d17.069333116092324!3d51.07925587956709!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470fc230d39c7975%3A0x8dd30400390039f3!2sEcoshine!5e0!3m2!1spl!2spl!4v1498588010384"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    <?php } ?>

</main>
<?php get_footer('carcare'); ?>

array(17) {
["ID"]=> int(1885)
["id"]=> int(1885)
["title"]=> string(4) "Test"
["filename"]=> string(8) "Test.mp4"
["url"]=> string(59) "https://www.ecoshine.pl/wp-content/uploads/2019/03/Test.mp4"
["alt"]=> string(0) ""
["author"]=> string(1) "1"
["description"]=> string(0) ""
["caption"]=> string(0) ""
["name"]=> string(4) "test"
["date"]=> string(19) "2019-03-13 15:52:10"
["modified"]=> string(19) "2019-03-13 15:52:10"
["mime_type"]=> string(9) "video/mp4"
["type"]=> string(5) "video"
["icon"]=> string(58) "https://www.ecoshine.pl/wp-includes/images/media/video.png"
["width"]=> int(1280)
["height"]=> int(720) }
