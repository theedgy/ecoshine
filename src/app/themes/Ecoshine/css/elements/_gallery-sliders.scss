.gallery-sliders {
	display: flex;
	justify-content: space-around;
	flex-wrap: wrap;
	.sliders-single {
		width: 48%;
		margin-top: 2vw;
		position: relative;
		@include whiteBox();
		background-color: rgba(#000, 0.45);
		@include mq(md) {
			width: 100%;
		}
	}
	.slider-image {
		height: 400px;
		background-repeat: no-repeat;
		background-position: center;
		background-size: cover;
		@include mq(xs) {
			height: 200px;
		}
	}
	.slider-title {
		text-align: center;
		padding: 1vw 0;
		text-transform: uppercase;
		margin: 0;
		color: $color_green;
		color: #fff;
		z-index:2;
		@include mq(sm) {
			padding: 2vw 1vw;
		}
	}
	.slider-single {

		@include mq(sm) {
			padding: 0;
			padding-bottom: 0;
		}
		.slick-track{
			@include mq(sm) {
				display: flex !important;
				align-items: center;
			}
		}
		img {
			height: 95%;
			max-height: 300px;
			width: auto;
			margin: 0 auto;
			box-shadow: 2px 2px 2px rgba(#000, 0.5);
			@include mq(sm) {
				height: auto;
				max-height: none;
				object-fit: cover;
				font-family: 'object-fit: cover';
			}
		}
		.slick-slide {
			@include mq(sm) {
				display: flex;
				justify-content: center;
				align-items: center;
			}
		}
	}

	.slider-nav {
		height: 100px;
		position: absolute;
		bottom: 4px;
		left: 0;
		right: 0;
		display: flex;
		align-items: flex-end;
		opacity: 0.5;
		transition: opacity 0.2s;
		cursor: pointer;
		@include mq(sm) {
			opacity: 1;
		}
		&:hover {
			opacity: 1;
		}
		@include mq(sm) {
			height: auto;
			position: relative;
			margin-bottom: 4px;
			margin-top: 4px;
			bottom: 0;
		}
		img {
			height: 100%;
			max-height: 100px;
			max-width: 100px;
			margin: 0 2px;
		}
		.slick-track {
			display: flex;
			align-items: flex-end;
		}
	}
	.slick-arrow {
		-webkit-appearance: none;
		border: 0;
		outline: 0;
		background-color: transparent;
		background: transparent;
		display: flex;
		align-items: center;
		justify-content: center;
		font-size: 0;
		position: absolute;
		left: 0;
		cursor: pointer;
		z-index: 99;
		@include center-vrt();
		&:before {
			content: '\276C';
			display: block;
			font-size: 70px;
			transition: all 0.3s;
			color: rgba(#fff, 0.6);
			text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.3);
		}
		&.slick-next {
			right: 0;
			left: auto;
			&:before {
				content: '\276D';
			}
		}
		&:hover {
			&:before {
				color: $color_green;
			}
		}
	}
}