<?php
/**
 * The archive template file.
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */
get_header();
?>
	<main class="content content-archive">

		<?php if (have_posts()) : ?>

			<?php get_template_part('loop', 'index'); ?>

		<?php else: ?>

			<h2><?php _e('Sorry, nothing found.','Ecoshine'); ?></h2>

		<?php endif;  ?>

	</main>

<?php get_template_part('sidebar') ?>


<?php get_footer(); ?>