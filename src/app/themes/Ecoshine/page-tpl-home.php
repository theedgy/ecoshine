<?php
/**
 * Template Name: Szablon główny
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */

get_header();
the_post(); ?>

	<main class="content full-width">

		<?php get_theme_part('site-elements/cennik'); ?>

		<div class="content-container animated">
			<?php the_content(); ?>
		</div>


		<?php
		$slidery = get_field('slidery');
		if ($slidery) { ?>

			<div class="home-sliders">
				<div class="slider-testimonials animated">
					<div class="slider-scroll">
						<?php
						foreach ($slidery[0]['slider'] as $sliderField) {
							echo '<blockquote class="slider-single blockquote">';
							echo '<p class="blockquote-content">' . $sliderField['tekst_glowny'] . '</p>';
							echo '<span class="blockquote-author">' . $sliderField['tekst_alternatywny'] . '</span>';
							echo '</blockquote>';
						}
						?>
					</div>
				</div>

				<div class="slider-icons animated">
					<div class="slider-icons--wrapper">
						<span class="slider-title"><?php echo $slidery[1]['nazwa_slidera']; ?></span>
						<div class="slider-sprzet">
							<?php
							foreach ($slidery[1]['slider'] as $sliderField) {
								echo '<div class="slider-single">';
								echo wp_get_attachment_image($sliderField['obrazek']['id'], 'logos-slider');
								echo '</div>';
							}
							?>
						</div>
					</div>
					<div class="slider-icons--wrapper">
						<span class="slider-title"><?php echo $slidery[2]['nazwa_slidera']; ?></span>
						<div class="slider-srodki">
							<?php
							foreach ($slidery[2]['slider'] as $sliderField) {
								echo '<div class="slider-single">';
								echo wp_get_attachment_image($sliderField['obrazek']['id'], 'logos-slider');
								echo '</div>';
							}
							?>
						</div>
					</div>

				</div>

			</div>
		<?php } ?>

		<?php if (is_page(12)) { ?>
			<div class="contact-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2506.5825405226574!2d17.069333116092324!3d51.07925587956709!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470fc230d39c7975%3A0x8dd30400390039f3!2sEcoshine!5e0!3m2!1spl!2spl!4v1498588010384"
				        frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		<?php } ?>

	</main>

<?php get_footer(); ?>
