var $ = jQuery.noConflict();

var Project = {

    init: function () {
        $('html').removeClass('no-js');
        objectFitImages('img');
    },

    socialPanels: function () {

        $('.tab-click').on('click', function () {
            $(this).toggleClass('active');
        });

    },

    slickInit: function () {

        $('.flow').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            appendArrows: $('.slick-navigation'),
            variableWidth: true,
            centerMode: true,
            speed: 200,
            initialSlide: 2,
        });

        $('.slider-scroll').slick({
            arrows: false,
            dots: false,
            speed: 1000,
            autoplay: true,
            autoplaySpeed: 3500,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: false,
            pauseOnHover: true,
        });

        $('.slider-sprzet').slick({
            arrows: false,
            dots: false,
            speed: 1000,
            autoplay: true,
            fade: true,
            autoplaySpeed: 1000,
            slidesToScroll: 1,
        });
        $('.slider-srodki').slick({
            arrows: false,
            dots: false,
            speed: 1000,
            autoplay: true,
            fade: true,
            autoplaySpeed: 1000,
            slidesToScroll: 1,
        });
        $('.carcare-image--slider').slick({
            arrows: false,
            dots: true,
            speed: 500,
            autoplay: true,
            fade: true,
            autoplaySpeed: 2500,
            slidesToScroll: 1,
            slidesToShow: 1,
        });


        for (var index = 1; index < 5; index++) {
            $('.slider-single-' + index).slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                fade: true,
                asNavFor: '.slider-nav-' + index
            });
            $('.slider-nav-' + index).slick({
                slidesToShow: 10,
                slidesToScroll: 1,
                asNavFor: '.slider-single-' + index,
                dots: false,
                arrows: false,
                centerMode: true,
                focusOnSelect: true,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1
                    }
                }
                ]
            });
        }
    },

    lightbox: function () {

        $('a[data-lightbox="l-gallery"], .post a[href*="jpg"], .post a[href*="JPG"], .post a[href*="png"]').swipebox({
            useCSS: true, // false will force the use of jQuery for animations
            useSVG: true, // false to force the use of png for buttons
            initialIndexOnArray: 0, // which image index to init when a array is passed
            hideCloseButtonOnMobile: false, // true will hide the close button on mobile devices
            removeBarsOnMobile: false, // false will show top bar on mobile devices
            hideBarsDelay: 3000, // delay before hiding bars on desktop
            videoMaxWidth: 1140, // videos max width
            beforeOpen: function () {
            }, // called before opening
            afterOpen: null, // called after opening
            afterClose: function () {
            }, // called after closing
            loopAtEnd: false // true will return to the first image after the last image is reached
        });

        // lightbox.option({
        // 	'resizeDuration': 200,
        // 	'wrapAround': true
        // });

    },
    animate: function () {
        $('.animated').addClass('visible zoomIn');
        $('.carcare-single:odd').addClass('visible fadeInLeft');
        $('.carcare-single:even').addClass('visible fadeInRight');
    },
    svgReplace: function () {
        $('img.svg').each(function () {
            var $img = $(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');
            var imgStyle = $img.attr('style');

            $.get(imgURL, function (data) {
                var $svg = $(data).find('svg');

                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }
                if (typeof imgStyle !== 'undefined') {
                    $svg = $svg.attr('style', imgStyle);
                }

                $svg = $svg.removeAttr('xmlns:a');

                $img.replaceWith($svg);

            }, 'xml');
        });
    },
    headerFixed: function () {
        var $menu = $('header.main').find('.main-menu');

        function checkScrolltop() {
            if ($(window).scrollTop() > 10) {
                $menu.addClass('fixed');
            } else {
                if (!$('body').hasClass('carcare')) {
                    $menu.removeClass('fixed');
                }

            }
        }

        checkScrolltop();

        $(window).on('scroll', function () {
            checkScrolltop();
        })
    },
    stopLoading: function () {
        $('.loading-overlay').addClass('invisible');
    },
    loading: function () {
        $('.loading-overlay').removeClass('invisible');
    },
    Maps: function () {

        function new_map($el) {

            // var
            var $markers = $el.find('.marker');


            // vars
            var args = {
                zoom: 16,
                scrollwheel: false,
                center: new google.maps.LatLng(0, 0),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                style: [
                    {
                        "featureType": "administrative",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#444444"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [
                            {
                                "saturation": -100
                            },
                            {
                                "lightness": 45
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffd400"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "hue": "#ff0000"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "color": "#c2c4c4"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    }
                ]
            };


            // create map
            var map = new google.maps.Map($el[0], args);


            // add a markers reference
            map.markers = [];


            // add markers
            $markers.each(function () {

                add_marker($(this), map);

            });


            // center map
            center_map(map);


            // return
            return map;

        }

        function add_marker($marker, map) {

            // var
            var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

            // create marker
            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });

            // add to array
            map.markers.push(marker);

            // if marker contains HTML, add it to an infoWindow
            if ($marker.html()) {
                // create info window
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html()
                });

                // show info window when marker is clicked
                google.maps.event.addListener(marker, 'click', function () {

                    infowindow.open(map, marker);

                });
            }

        }

        function center_map(map) {

            // vars
            var bounds = new google.maps.LatLngBounds();

            // loop through all markers and create bounds
            $.each(map.markers, function (i, marker) {

                var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

                bounds.extend(latlng);

            });

            // only 1 marker?
            if (map.markers.length == 1) {
                // set center of map
                map.setCenter(bounds.getCenter());
                map.setZoom(16);
            } else {
                // fit to bounds
                map.fitBounds(bounds);
            }

        }

        var map = null;

        $(document).ready(function () {

            $('.acf-map').each(function () {

                // create map
                map = new_map($(this));

            });

        });
    },
    menuResponsive: function () {
        $('.responsive-menu-trigger').on('click', function (el) {
            el.preventDefault();
            $('#menu-menu-glowne-carcare, #menu-menu-glowne, .main-menu__socials').toggleClass('visible');
        });
        $('.menu-item-has-children > a').on('click', function (el) {
            el.preventDefault();
            $(this).siblings('.sub-menu').toggleClass('active');
        });
        $('.menu-item:not(.menu-item-has-children)').bind('click', function (e) {
            window.location.href = $(this).find('a').first().attr('href');
        })
    },
    videoPlay: function () {
        if (!$('.hero-image').hasClass('not-front')) {
            $('.hero-image').addClass('invisible');
        }
    },

    sidebarTrigger: function () {
        $trigger = $('.widget-area-trigger');
        $trigger.on('click', function () {
            $(this).parent().toggleClass('active');
        });
    },
};

var jsAccordeon = {

    init: function () {
        this.cacheDOM();
        this.bindEvent();
        this.hideAll();
    },

    // Get elements
    cacheDOM: function () {
        // this.$accordeon = $('.accordeon-wrapper');
        this.$element = $('.accordion-element');
        this.$trigger = this.$element.find('.accordion-element__title');
    },

    // Bind actions
    bindEvent: function () {
        this.$trigger.on('click', this.showContent);
    },

    // Methods
    showContent: function () {
        var $element = $(this).parent('.accordion-element'),
            $content = $element.find('.accordion-element__content');

        if ($element.hasClass('active')) {
            jsAccordeon.hide.call($element);
        } else {
            // jsAccordeon.hideAll();
            jsAccordeon.show.call($element);
        }
    },

    hide: function () {
        $(this).removeClass('active');
    },

    show: function () {
        $(this).addClass('active');
    },

    hideAll: function () {
        this.$element.each(this.hide);
    }
};

function showAll() {
    textAll = $("#text-all");
    moreText = $("#btn-text-all");
    moreText.on('click', function () {
        console.log(textAll);
        if (!(textAll.is(':visible'))) {
            textAll.show(500);
            moreText.html("Mniej");
        } else {
            textAll.hide(500);
            moreText.html("Więcej");
        }
    });

}

showAll();


function videoLoad () {
    var $wrapper  = $('.carcare-image');
    var $video = $wrapper.find('video');

    if ($video.length > 0) {
        var videoHeight = parseInt($video.attr('height'));
        var videoWidth = parseInt($video.attr('width'));
        var wrapperHeight = parseInt($wrapper.height());
        var wrapperWidth = parseInt($wrapper.width());
        var videoAR = videoWidth / videoHeight;
        var wrapperAR = wrapperWidth / wrapperHeight;

        if (wrapperAR > videoAR) {
            $video.width(wrapperWidth);
            $video.height(wrapperWidth * videoAR);
        } else {
            $video.height(wrapperHeight);
            $video.width(wrapperHeight * videoAR);
        }

        $video.addClass('changed')
    }
}

$(document).ready(function () {
    Project.init();
    Project.menuResponsive();
    Project.Maps();
    Project.headerFixed();
    Project.socialPanels();
    Project.lightbox();
    Project.svgReplace();
    Project.animate();
    Project.sidebarTrigger();
    Project.sidebarTrigger();
    jsAccordeon.init();


    if ($('.video-header-video').is(':visible')) {
        $(window).on('load', function () {
            Project.videoPlay();
        });
    }

});

$(window).on('load', function () {
    Project.slickInit();
    Project.stopLoading();
    videoLoad();
});
