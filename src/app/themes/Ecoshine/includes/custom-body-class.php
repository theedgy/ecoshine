<?php

function custom_body_class($s_class = null)
{
	$classes = get_body_class();
	$s_classes_text = '';
	foreach ($classes as $class) {
		$new_class = str_replace('page-template-', '', $class);
		$new_class = str_replace('page-tpl-', '', $new_class);
		$new_class = str_replace('-php', '', $new_class);
		$s_classes_text .= ' ' . $new_class;
	}
	return trim($s_classes_text) . ' ' . $s_class;
}