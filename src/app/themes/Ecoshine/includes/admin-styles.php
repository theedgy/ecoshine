<?php

function admin_style()
{
	wp_enqueue_style('admin-styles', get_template_directory_uri() . '/admin-styles.css');
}

add_action('admin_enqueue_scripts', 'admin_style');

