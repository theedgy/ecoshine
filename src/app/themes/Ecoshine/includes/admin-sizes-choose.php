<?php

add_filter('image_size_names_choose', 'ml_custom_image_choose');
function ml_custom_image_choose($args)
{

	global $_wp_additional_image_sizes;

	// make the names human friendly by removing dashes and capitalising
	foreach ($_wp_additional_image_sizes as $key => $value) {
		$custom[$key] = ucwords(str_replace('-', ' ', $key));
	}

	return array_merge($args, $custom);
}
