<?php

function my_mce4_options($init)
{

	$custom_colours = '
        "a5ce3a", "Zielony",
        "fde434", "Żółty",
        "414042", "Szary",
        "000000", "Czarny",
        "ffffff", "Biały"
    ';

	// build colour grid default+custom colors
	$init['textcolor_map'] = '[' . $custom_colours . ']';

	// change the number of rows in the grid if the number of colors changes
	// 8 swatches per row
	$init['textcolor_rows'] = 1;

	return $init;
}

add_filter('tiny_mce_before_init', 'my_mce4_options');
