<?php
/**
 * Loop for home page.
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */
?>
<section class="posts">
	<?php while (have_posts()) : the_post(); ?>
		<article class="post">

			<header>
				<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<span class="date"><?php the_date(); ?></span>
				<span class="author"><?php echo get_the_author(); ?></span>
			</header>

			<div class="post-entry">
				<?php the_content('Czytaj więcej...'); ?>
			</div>

		</article>
		<hr>
	<?php endwhile; ?>
</section>