<?php
/**
 * The Header for CarCare theme.
 *
 * Displays all of the <head> section and page header
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */
?>
<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">


<head>
	<?php
	get_theme_part( 'html-head' );
	get_theme_part( 'header/facebook-pixel-autodetailing' ); ?>

    <style>
        body.carcare:before {
            background-image: url(<?php echo wp_get_attachment_image_src(get_field('tlo_strony_carcare', 'options')['id'], 'background')[0] ?>);
        }
    </style>
</head>

<body class="<?php echo custom_body_class(); ?>">

<?php get_theme_part('header/google-tag-manager-body'); ?>
<?php get_theme_part('widgets/facebook-code'); ?>

<div id="page">

    <header class="main header-carcare">
        <div class="container">

            <nav class="main-menu fixed">

                <a href="<?php the_field( 'link_do_strony_glownej_autodetailingu', 'options' ); ?>" class="main-menu__logo">
					<?php echo wp_get_attachment_image( get_field( 'logo_carcare', 'options' )['id'], false, false, array( 'class' => 'svg' ) ); ?>
                </a>

                <a href="#" class="responsive-menu-trigger"></a>
				<?php wp_nav_menu( array( "theme_location" => 'carcare', 'container' => false ) ); ?>

                <ul class="main-menu__socials">
					<?php
					$socials = get_field( 'socialki_autodetailing', 'options' );
					foreach ( $socials as $social ) { ?>

                        <li>
                            <a rel="nofollow" href="<?php echo $social['link'] ?>" class="main-menu__socials-single" target="_blank"
                               title="<?php echo $social['nazwa'] ?>">
								<?php echo wp_get_attachment_image( $social['ikona']['id'], false, false, array( 'class' => 'svg' ) ); ?>
                            </a>


							<?php if ( isset( $social['kod_dla_okna'] ) && ( $social['kod_dla_okna'] !== '' ) ) : ?>
                                <div class="main-menu__socials-wrapper">
									<?php echo $social['kod_dla_okna'] ?>
                                </div>
							<?php endif; ?>

                        </li>

					<?php } ?>

                </ul>
            </nav>
        </div>
    </header>
    <div class="loading-overlay">
        <div class='uil-default-css' style='transform:scale(0.65);'>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
            <div style='top:80px;left:93px;width:14px;height:40px;background:#fde434;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;'></div>
        </div>

    </div>

    <section class="main">
        <div class="container">
