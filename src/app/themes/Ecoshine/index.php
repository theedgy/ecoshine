<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */
get_header();
?>
<?php $sHeadText = null; ?>
<?php if(is_archive()) { 
	if(is_day()) :
		$sHeadText = __('Daily Archives:', 'Ecoshine') . get_the_date();
	elseif(is_month()) :
		$sHeadText = __('Monthly Archives:', 'Ecoshine') . get_the_date(_x('F Y', 'monthly archives date format', 'Ecoshine'));
	elseif(is_year()) :
		$sHeadText = __('Yearly Archives:', 'Ecoshine') . get_the_date(_x('Y', 'yearly archives date format', 'Ecoshine'));
	else :
		$sHeadText = __('Blog Archives', 'Ecoshine');
	endif;
} ?>
<?php if(is_category()) { 
	$sHeadText = __("Category:", "Ecoshine").' '.single_cat_title( '', false ); 
} ?>
<?php if(is_author()) { 
	$authordata = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
	$sHeadText = __("Author:", "Ecoshine").' '.$authordata->display_name; 
} ?>
<?php if(is_tag()) { 
	$sHeadText = __("Tag:", "Ecoshine").' '.single_tag_title( '', false ); 
} ?>
<?php if(is_search()) { 
	$sHeadText = __("Wyszukiwana fraza:", "Ecoshine").' '.get_search_query();
} ?>

			<main class="content">
				<?php if (have_posts()) : ?>

					<?php if ($sHeadText) { ?>
						<h1><?php echo $sHeadText; ?></h1>
					<?php } ?>

					<?php get_template_part('loop', 'index'); ?>

				<?php else: ?>
					
					<h2><?php _e('Sorry, nothing found.','Ecoshine'); ?></h2>

				<?php endif;  ?>

			</main>

			<?php get_template_part('sidebar') ?>

			<?php
			$args = array(
	            'mid_size'           => 3,
	            'prev_text'          => __( 'Prev' ),
	            'next_text'          => __( 'Next' ),
	            'screen_reader_text' => __( 'Posts navigation' )
	        )
			?>
			<?php the_posts_pagination($args); ?>

<?php get_footer(); ?>