<?php
/**
 * The Header for theme.
 *
 * Displays all of the <head> section and page header
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
    <?php
    get_theme_part('html-head');
    get_theme_part('header/facebook-pixel-ecoshine');
    ?>
</head>

<body class="<?php echo custom_body_class(); ?>">

<?php get_theme_part('header/google-tag-manager-body'); ?>
<?php get_theme_part('widgets/facebook-code'); ?>

<div id="page">

    <header class="main">
        <div class="container">

            <nav class="main-menu">

                <a href="<?php echo home_url(); ?>" class="main-menu__logo">
					<?php echo wp_get_attachment_image(get_field('logo_dla_ecoshine', 'options')['id'], false, false, array('class' => 'svg')); ?>
                </a>

                <a href="#" class="responsive-menu-trigger"></a>

                <?php wp_nav_menu(array("theme_location" => 'primary', 'container' => false)); ?>

                <ul class="main-menu__socials">
                    <?php
                    $socials = get_field('socialki', 'options');
                    foreach ($socials as $social) { ?>

                        <li>
                            <a rel="nofollow" href="<?php echo $social['link'] ?>" class="main-menu__socials-single" target="_blank"
                               title="<?php echo $social['nazwa'] ?>">
                                <?php echo wp_get_attachment_image($social['ikona']['id'], false, false, array('class' => 'svg')); ?>
                            </a>
                            <?php if (isset($social['kod_dla_okna']) && ($social['kod_dla_okna'] !== '')) : ?>
                                <div class="main-menu__socials-wrapper">
                                    <?php echo $social['kod_dla_okna'] ?>
                                </div>
                            <?php endif; ?>
                        </li>

                    <?php } ?>

                </ul>
            </nav>

			<?php
			get_theme_part('site-elements/hero');
			get_theme_part('site-elements/title');
			?>

        </div>
    </header>

    <section class="main">
        <div class="container">
