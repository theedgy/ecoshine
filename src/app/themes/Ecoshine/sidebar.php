<?php
/**
 * Primary widget area
 *
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */
?>
<aside class="widget-area">
	<?php if (is_active_sidebar('primary_widget_area')) : ?>
		<span class="widget-area-trigger"><?php _e('Na skróty', 'Ecoshine'); ?></span>
		<?php dynamic_sidebar('primary_widget_area'); ?>
	<?php endif; ?>
</aside>