<?php
/**
 * The footer for CarCare theme.
 *
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */
?>

</div>
<!-- .container -->
</section>
<!-- /.main -->
<footer class="main footer-carcare">
	<div class="container">

		<div class="footer-column">

			<?php get_theme_part('footer/ad-footer-1') ?>

		</div>

		<div class="footer-column">

			<?php get_theme_part('footer/ad-footer-2') ?>

		</div>

		<div class="footer-column mobile-hidden">

			<?php get_theme_part('footer/ad-footer-3') ?>

		</div>

		<div class="footer-column">

			<?php get_theme_part('footer/ad-footer-4') ?>

		</div>

	</div>
	<?php get_theme_part('footer/copyrights') ?>
</footer>
</div>
<!-- /#page -->
<?php wp_footer(); ?>
<?php get_theme_part('widgets/call'); ?>
</body>

</html>
