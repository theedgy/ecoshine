<?php
/**
 * The static page template.
 *
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */

get_header();
the_post(); ?>

	<main class="content full-width">

		<section class="content-container animated">

			<h1><?php the_title(); ?></h1>

			<?php the_content(); ?>

		</section>

	</main>

<?php get_footer(); ?>