<?php
/**
 * Template Name: Szablon Grid
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */

get_header(); the_post(); ?>

    <main class="content full-width">

        <?php $kolumny = get_field('kolumny'); ?>
        <?php $elementy = get_field('elementy'); ?>

        <div class="custom-grid">

            <?php foreach ($elementy as $element) { ?>
            <div class="custom-column custom-column-<?php echo $kolumny ?>">
                <?php
                    switch ($element['typ_elementu']) {
                        case 'text':
                            echo $element['text'];
                            break;
                        
                        case 'lightbox': ?>
                                  <a href="<?php echo $element['obrazek_lighbox']['url'] ?>" data-lightbox="l-gallery" <?php echo $element['opcjonalny_tytul'] ? 'data-title="'.$element[ 'opcjonalny_tytul']. '"' : '' ?>>
                                <?php echo wp_get_attachment_image($element['obrazek_lighbox']['ID'], 'slider-duza')?>
                                </a>
                                <?php if ($element[ 'opcjonalny_tytul']) {
                                        ?> <span class="image-description"><?php echo $element[ 'opcjonalny_tytul'] ?></span>
                                <?php
                                    }
                            break;
                        
                        case 'ico':
                              echo wp_get_attachment_image($element['obrazek_zwykly']['ID']);
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                    ?>
            </div>
            <?php } ?>
        </div>
    </main>

    <?php get_footer(); ?>
