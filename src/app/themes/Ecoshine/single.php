<?php
/**
 * The single post page template.
 *
 *
 * @package WordPress
 * @subpackage Ecoshine
 * @since Ecoshine 1.0
 */

get_header();
the_post(); ?>

	<main class="content content-container animated single-post-content">


		<section class="entry">
			<header>
				<h1><?php the_title(); ?></h1>
				<span class="date"><?php the_date(); ?></span>
				<span class="author"><?php echo get_the_author(); ?></span>
			</header>

			<?php the_content(); ?>

		</section>

	</main>
<?php get_template_part('sidebar') ?>

<?php get_footer(); ?>