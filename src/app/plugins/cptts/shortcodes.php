<?php

if (!class_exists('Cptts_Shortcodes')):

class Cptts_Shortcodes {

	private $shortcodes = array(
		'custom_button' => 'c_button',
		'clear_func' => 'clear',
		'tabs_func' => 'tabs',
		'tab_func' => 'tab',
		'slider_func' => 'slider',
		'slider_single' => 'slider_item',
		'accordion_func' => 'accordion',
	);

	
		function accordion_func($atts, $content = null)
	{

		extract(shortcode_atts(array(
			'title' => ''
			), $atts));

		return "<div class='accordion-element'><div class='accordion-element__title'><span>$title</span><span class='accordion-icon'></span></div><div class='accordion-element__content'>". do_shortcode($content) ."</div></div>";

	}
	function slider_func($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'id' => '',
			'class' => ''), $atts));

		$html = '<div id="slider-'.$id.'" class="slider-'.$class.'">';

		$html .= do_shortcode($content);

		$html .= '</div>';

		return $html;
	}
	function slider_single($atts, $content = null)
	{

		$html = '<div class="slider-single">';

		$html .= do_shortcode($content);

		$html .= '</div>';

		return $html;
	}

	function custom_button($atts)
	{
		extract(shortcode_atts(array(
			'href' => '#',
			'label' => __('Anchor', 'cptts'),
		), $atts));

		return "<a href='$href' class='custom-btn'>$label</a>";
	}

	function clear_func($atts, $content = "")
	{
		return "<div class='clearfix'></div>";
	}

	function tabs_func($atts, $content = null)
	{
		extract(shortcode_atts(array('titles' => ''), $atts));

		$titles = explode(",", $titles);
		$html = '<div class="tabs">';

		$html .= '<ul>';
		$i=1;
		foreach($titles as $title):
			$html .= '<li><a href="#tabs-'.$i.'" rel="tabs-'.$i.'">'.trim($title).'</a></li>';
			$i++;
		endforeach;

		$html .= '</ul>';
		$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;
	}

	function tab_func($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'id' => ''), $atts));

		$html = '<div id="tabs-'.$id.'">';

		$html .= do_shortcode($content);

		$html .= '</div>';

		return $html;
	}

	/**
	 * INTERNAL CLASS FUNCTIONALITY
	 */

	/**
	 * Cptts_Shortcodes constructor.
     */
	function __construct()
	{
		add_action( 'init', array( $this, 'create_shorcodes' ) );
	}

	/**
	 * Registers all shortcodes defined in $shortcodes property.
     */
	public function create_shorcodes()
	{
		foreach ($this->shortcodes as $func => $name) {
			add_shortcode($name, array($this, $func));
		}
	}
}

new Cptts_Shortcodes();

endif;
