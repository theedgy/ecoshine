let fs           = require('fs-extra');
let Archive      = require('../../lib/Archive');
let isProduction = require('../../lib/isProduction');
let config       = require('../../lib/getProjectConfig');
let fileExists   = require('../../lib/fileExists');

module.exports = {
  default: cb => {
    let archive = new Archive(config('paths.public'), 'filepack.zip');

    // Public folder contents
    archive.archive.glob('**/*', {
      cwd: config('paths.public'),
      ignore: ['filepack.zip', 'qa-summary.html']
    });

    // Sources, if configured to include sources
    if (true === config('project.sources')) {
      archive.add(['**/*.{scss,sass}'], config('paths.source'), 'sources/styles');
    }

    if (true === config('project.sources') || config('components.webpack')) {
      archive.add(['**/*.{js,jsx}'], config('paths.source'), 'sources/js');
    }

    archive.save();
    cb();
  }
};
