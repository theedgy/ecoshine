/*
 |--------------------------------------------------------------------------
 | Generate .htaccess
 |--------------------------------------------------------------------------
 |
 | .htaccess file is required by WordPress to recognize custom URLs.
 | This generates the .htaccess file in the public directory that is based
 | on default WordPress .htaccess, but with paths based on .env config.
 |
 */
let fs         = require('fs-extra');
let fileExists = require('../../lib/fileExists');
let config     = require('../../lib/getProjectConfig');
let _          = require('underscore');
let url        = require('url');

module.exports = cb => {
  let templatePath = 'env/configs/.htaccess.tpl';
  if (!fileExists(templatePath) || !process.env.APP_URL) {
    cb();
    return;
  }

  let template = _.template(fs.readFileSync(templatePath).toString());
  let urlData  = url.parse(process.env.APP_URL);

  fs.writeFileSync(`${config('paths.public')}/.htaccess`, template({
    path: urlData.path,
  }));

  cb();
};
