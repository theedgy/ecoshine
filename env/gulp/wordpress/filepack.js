let fs           = require('fs-extra');
let Archive      = require('../../lib/Archive');
let isProduction = require('../../lib/isProduction');
let config       = require('../../lib/getProjectConfig');
let dump         = require('../../lib/dbDump');
let fileExists   = require('../../lib/fileExists');

module.exports = {
  default: cb => {
    let archive = new Archive(config('paths.public'), 'filepack.zip');

    // Themes
    let themes = fs.readdirSync(`${config('paths.public')}/app/themes`);
    themes.forEach(theme => {
      archive.archive.directory(
        `${config('paths.public')}/app/themes/${theme}`,
        `themes/${theme}`
      );
    });

    // Plugins
    if (fileExists(`${config('paths.public')}/app/plugins`)) {
      archive.archive.directory(
        `${config('paths.public')}/app/plugins`,
        'plugins'
      );
    }

    // Uploads
    if (fileExists(`${config('paths.source')}/app/uploads`)) {
      archive.archive.directory(
        `${config('paths.source')}/app/uploads`,
        'uploads'
      );
    }

    // Installation instructions
    if (fileExists('resources/How-to-install.pdf')) {
      archive.archive.file(
        'resources/How-to-install.pdf',
        {name: 'How-to-install.pdf'}
      );
    }

    // Sources, if configured to include sources
    themes.forEach(theme => {
      if (true === config('project.sources')) {
        archive.add(
          [ '**/*.{scss,sass}' ],
          `${config('paths.source')}/app/themes/${theme}`,
          `sources/${theme}/styles`
        );
      }

      if (true === config('project.sources') || config('components.webpack')) {
        archive.add(
          [ '**/*.{js,jsx}' ],
          `${config('paths.source')}/app/themes/${theme}`,
          `sources/${theme}/js`
        );
      }
    });

    // If we're using webpack we need to throw webpack config and
    // package.json into the filepack.
    if (config('components.webpack')) {
      archive.archive.file('webpack.config.js', { name: 'configs/webpack.config.js' });

      // package.json
      // ---------------------------------------------
      let contents = fs.readFileSync('package.json').toString();
      contents = JSON.parse(contents);

      // Don't leave any references to Chop-Chop in final package.json
      contents.name = '';

      // Remove any scripts other than webpack-related
      const scripts = contents.scripts;
      for (script in scripts) {
        if (!script.match(/^webpack/)) {
          delete scripts[script];
        }
      }

      // Remove all environment dependencies, except those required
      // to compile things with webpack.
      const devDependencies = contents.devDependencies;
      for (dep in devDependencies) {
        // Keep webpack
        if ('webpack' === dep) {
          continue;
        }

        // Keep transpilers
        if (dep.match(/^babel/)) {
          continue;
        }

        // Remove everything else
        delete devDependencies[dep];
      }

      archive.archive.append(JSON.stringify(contents, null, 4), { name: 'configs/package.json' });
    }

    // Database dump
    archive.archive.file('resources/dumps/remote.sql.tpl', { name: 'db.sql.tpl' });

    archive.save();
    cb();
  }
};
