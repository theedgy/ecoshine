/*
 |--------------------------------------------------------------------------
 | WordPress build
 |--------------------------------------------------------------------------
 |
 | Specify tasks that should be running during
 | wordpress project building
 |
 */
let runSequence = require('run-sequence');

module.exports = callback => {
    runSequence(
      'install',

      'clean',
      'htaccess',
      'copy:source',
      'copy:uploads-to-public',
      'copy:third-party',
      'copy:dev-tools',

      'compile',
      'lint',

      callback
    );
};
