let shell        = require('shelljs');
let fileExists   = require('../../lib/fileExists');
let config       = require('../../lib/getProjectConfig');
let isProduction = require('../../lib/isProduction');

module.exports = {

  /**
   * Run composer install.
   * If the composer executable is not present, it will be installed first.
   */
  default: () => {
    if (config('project.type') !== 'wordpress' || isProduction()) {
      return;
    }

    if (!fileExists('composer.phar')) {
      shell.exec('php -r "readfile(\'https://getcomposer.org/installer\');" | php');
    }

    shell.exec('php composer.phar install');
  }
};
