/*
 |--------------------------------------------------------------------------
 | Clean
 |--------------------------------------------------------------------------
 |
 | Clears the contents of the public directory.
 | Used at the beginning of the build to prepare for a clean build.
 |
 */

let fs         = require('fs-extra');
let path       = require('path');
let fileExists = require('../../lib/fileExists');
let gulp       = require('gulp');
let $          = require('gulp-load-plugins')({ lazy: true });
let config     = require('../../lib/getProjectConfig');

/**
 * Remove all files in the folder that don't exist in the source directory.
 *
 * @param {string} folder - Name of the folder to clean up.
 * @param {string} source - Source folder to check for files.
 * @param {string} out    - Output folder, where are deleting files from.
 */
function cleanUp(folder, source, out) {
  let files = fs.readdirSync(folder);
  files.forEach(file => {
    let fullPath = path.join(folder, file);
    let stat = fs.statSync(fullPath);

    if (stat.isDirectory()) {
      cleanUp(fullPath, source, out);
      return;
    }

    let sourceFile = path.join(source, path.relative(out, fullPath));
    if (!fileExists(sourceFile)) {
      fs.unlinkSync(fullPath);
    }
  });
}

module.exports = {
  default: cb => {
    let source = `${config('paths.source')}/app/themes`;
    let out = `${config('paths.public')}/app/themes`;

    cleanUp(out, source, out);
    cb();
  }
};
