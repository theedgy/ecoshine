/*
 |--------------------------------------------------------------------------
 | WordPress install
 |--------------------------------------------------------------------------
 |
 | Install Bedrock and WordPress, by copying them from vendor folder.
 |
 */

let runSequence  = require('run-sequence');
let isProduction = require('../../lib/isProduction');

module.exports = callback => {
  if (isProduction()) {
    runSequence(
      'wp:install-bedrock',
      callback
    );
  } else {
    runSequence(
      'composer',
      'wp:install-bedrock',
      'wp:db:create',
      callback
    );
  }
};
