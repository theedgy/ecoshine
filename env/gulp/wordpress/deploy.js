let runSequence = require('run-sequence');

module.exports = callback => {
  runSequence(
    'build',
    'wp:db:create',
    'wp:db:import',
    'filepack',
    'summary',
    callback
  );
};
