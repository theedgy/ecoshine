let shell           = require('shelljs');
let path            = require('path');
let config          = require('./getProjectConfig');
let messageFromFile = require('./messageFromFile');

/**
 * Run a wpcli command.
 * The function finds the path to wpcli, and to the WordPress install based on .env.
 * Only the command to be run needs to be supplied.
 *
 * @param {string}  command - wpcli command to run.
 * @param {boolean} silent  - If set to false errors will be outputted.
 */
module.exports = (command, silent = false) => {
  let wpHome = process.env.APP_URL;
  let wpSiteUrl = process.env.WP_SITEURL;
  if (!wpHome || !wpSiteUrl || '/wp' === wpSiteUrl) {
    messageFromFile('/env/messages/wp-env-not-configured');
    process.exit();
  }

  let wpPath = config('paths.public') + wpSiteUrl.replace(wpHome, '');

  let wpcliPath = path.normalize('vendor/bin/wp');
  shell.exec(`${wpcliPath} --path="${wpPath}" ${command}`, { silent });
};
