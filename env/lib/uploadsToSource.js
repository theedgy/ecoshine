let fs         = require('fs-extra');
let path       = require('path');
let config     = require('./getProjectConfig');
let fileExists = require('./fileExists');

module.exports = {

  /**
   * Copy the file from public uploads to source.
   *
   * @param {string} file - Path to the file added.
   */
  add: (file) => {
    file = path.relative(config('paths.public'), file);
    fs.copySync(
      `${config('paths.public')}/${file}`,
      `${config('paths.source')}/${file}`
    );
  },

  /**
   * Delete the file from the source, when file in public directory was deleted.
   *
   * @param {string} file - Path to deleted file.
   */
  delete: (file) => {
    file = path.relative(config('paths.public'), file);
    if (fileExists(`${config('paths.source')}/${file}`)) {
      fs.removeSync(`${config('paths.source')}/${file}`);
    }
  }
};
